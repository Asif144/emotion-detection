
import pandas as pd
import os
from collections import Counter

def readStopWords(fileName):
    stopWordList = []
    with open(fileName, encoding='utf-8') as f:
        for line in f:
            word = str(line)
            word = word.strip()
            stopWordList.append(word)

    return stopWordList

def readDataFrame(fileName):
    excelFile = pd.ExcelFile(fileName)
    sheetNameList = excelFile.sheet_names
    dataFrame = excelFile.parse(sheetNameList[0])
    return dataFrame

def wordCountInDataSet(dataFrame, columnName):
    wordList = []
    for index,row in dataFrame.iterrows():
        words = row[columnName].split()
        for word in words:
            wordList.append(word)
        #print(type(words))
    wordCounter = Counter(wordList)
    length = len(wordCounter)
    #print(len(wordCounter))
    #print(type(wordCounter))
    print(wordCounter.most_common(length))
    return
    
def readData(fileName):
    excelFile = pd.ExcelFile(fileName)
    print(excelFile.sheet_names)
    dataFrame = excelFile.parse('FirstPage')
    columnnames = dataFrame.columns
    print(columnnames)
    #find column value as a list
    col0 = list(dataFrame['EmotionalExpression'])
    print('col10=',col0)
    uniqueCol1Val = list(dataFrame.EmotionalExpression.unique())
    print((uniqueCol1Val))
    
    #unique value count
    print(dataFrame.EmotionalExpression.value_counts())
    '''
    #print rows
    for index,row in dataFrame.iterrows():
       print (row['EmotionalExpression'], row['Contents'])
    '''
    return
if __name__ == "__main__":
    dataFrame = readDataFrame("dataset.xlsx")
    wordCountInDataSet(dataFrame, 'Contents')
