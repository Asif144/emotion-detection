from sklearn.model_selection import train_test_split
from sklearn import svm
from sklearn.naive_bayes import GaussianNB, MultinomialNB
from sklearn.metrics import accuracy_score
from sklearn.metrics import classification_report

def splitDataset(classLabelList, dataList):
    trainData,testData, trainClass,testClass = train_test_split(dataList, classLabelList, test_size = 0.2,stratify=classLabelList)
    #print(testClass)
    print("Total data in train set = ", len(trainData))
    print("Total data in test set = ", len(testData))
    return trainData,testData, trainClass,testClass

def getLinearClassifier(trainData, trainClass):
    clfLinear = svm.LinearSVC(multi_class='ovr', max_iter=5000, C=.07)
    clfLinear.fit(trainData,trainClass)
    return clfLinear

def getRbfClassifier(trainData, trainClass):
    clfRbf = svm.SVC(kernel='rbf', gamma=0.7, C=1.0)
    clfRbf.fit(trainData,trainClass)
    return clfRbf

def getGaussianNaiveBayesClassifier(trainData, trainClass):
    clGaussianNB = GaussianNB()
    clGaussianNB.fit(trainData, trainClass)
    return clGaussianNB


def getMultinomialNaiveBayesClassifier(trainData, trainClass):
    clMultinomialNB = MultinomialNB()
    clMultinomialNB.fit(trainData, trainClass)
    return clMultinomialNB


def getPrediction(classifier, testDataList):
    predictions = classifier.predict(testDataList)
    return predictions



def accuracy(actualClass, predictedClass):
    acc = accuracy_score(actualClass, predictedClass)
    return acc

def getClassificationReport(actualClassList, predictedClassList, classNames):
    print("Full Classification Report:\n")
    print(classification_report(actualClassList, predictedClassList, target_names=classNames))
    return