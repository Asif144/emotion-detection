import preprocessingData as preDt
from sklearn.feature_extraction.text import CountVectorizer
def getRawInputSentence():
    print("\nEnter a sentence for prediction or -1 to break:")
    inputSentence = str(input())
    #print(inputSentence)
    return inputSentence

def getPreprocessedSentenceFromRawInput(inputSentence, stopWordList):
    preprocessedDataList =[]
    sentence = preDt.removePunctuation(inputSentence)
    finalSentence = preDt.removeStopWords(sentence, stopWordList)
    return finalSentence

def getTransformedVectorFromSentence(preprocessedSentence, mainVocabulary):
    vectorizer = CountVectorizer(analyzer='word', ngram_range=(1, 3), min_df=1, lowercase=False, token_pattern=u'[\S]+',
                    tokenizer=None, vocabulary=mainVocabulary)
    vector = vectorizer.transform([preprocessedSentence])
    vector = vector.toarray()
    vectorAsList = vector.tolist()
    #print(vectorAsList)
    return vectorAsList