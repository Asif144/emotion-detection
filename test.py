from sklearn.feature_extraction.text import CountVectorizer
import numpy as np
from sklearn import svm
corpus = [
   'This is the first document.',
    'This document is the second document.',
   'And this is the third one.',
   'Is this the first document?']
vectorizer = CountVectorizer()
X = vectorizer.fit_transform(corpus)
print(vectorizer.get_feature_names())
X = X.toarray()
print(np.squeeze(np.asarray(X)))
'''
X = X.tolist()
print(X)
'''
Y = [0, 1, 2, 3]
clf = svm.SVC( decision_function_shape='ovo')
clf.fit(X, Y)
print(clf.predict([[0 ,1 ,1, 1 ,0, 0, 1 ,0 ,1]]))