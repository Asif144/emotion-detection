import dataReader as DR
import preprocessingData as preDt
import classificationData as cfd
import processRawInput as pri
if __name__ == "__main__":
    #print('start')
    classNames =['আনন্দ', 'দুঃখ', 'বিরক্ত', 'অবাক', 'ভয়', 'রাগ','আবেগহীন']
    dataFrame = DR.readDataFrame("dataset.xlsx")
    stopWordList = DR.readStopWords("stopWordList.txt")
    #print(stopWordList)
    listOfListFromDF = preDt.convertDataFrameToListOfList(dataFrame)
    preprocessedData = preDt.getPreprocessedData(listOfListFromDF, stopWordList)
    classLabelList, dataList = preDt.getDataAndClassFromLL(preprocessedData)
    featuredDataList, mainVocabulary = preDt.featureExtraction(dataList)
    trainData, testData, trainClass, testClass = cfd.splitDataset(classLabelList,featuredDataList)

    #linear SVM
    linearClassifier = cfd.getLinearClassifier(trainData, trainClass)
    predictionList = cfd.getPrediction(linearClassifier, testData)
    cfd.getClassificationReport(testClass, predictionList, classNames)
    print("accuracy for linear SVM =", cfd.accuracy(testClass, predictionList))
   
    #nonlinear RBF SVM
    rbfClassifier = cfd.getRbfClassifier(trainData, trainClass)
    predictionList = cfd.getPrediction(rbfClassifier, testData)
    cfd.getClassificationReport(testClass, predictionList, classNames)
    print("accuracy for nolinear RBF SVM =", cfd.accuracy(testClass, predictionList))
    
    # Gaussian Naive Bayes
    gNaiveBayesClassifier = cfd.getGaussianNaiveBayesClassifier(trainData,trainClass)
    predictionList = cfd.getPrediction(gNaiveBayesClassifier, testData)
    cfd.getClassificationReport(testClass, predictionList, classNames)
    print("accuracy for Gaussian Naive Bayes =", cfd.accuracy(testClass, predictionList))

    # Multinomial Naive Bayes
    multinomialNaiveBayesClassifier = cfd.getMultinomialNaiveBayesClassifier(trainData,trainClass)
    predictionList = cfd.getPrediction(multinomialNaiveBayesClassifier, testData)
    cfd.getClassificationReport(testClass, predictionList, classNames)
    print("accuracy for Multinomial Naive Bayes =", cfd.accuracy(testClass, predictionList))

    # work with input data
    while 1:
        inputText = pri.getRawInputSentence()
        if inputText == "-1":
            break
        listOfListFromRawInput = [[inputText]]
        #print(listOfListFromRawInput)
        preprocessedSentence = pri.getPreprocessedSentenceFromRawInput(inputText, stopWordList)
        #print(preprocessedSentence)
        transformedVector = pri.getTransformedVectorFromSentence(preprocessedSentence, mainVocabulary)
        predictionList = cfd.getPrediction(linearClassifier, transformedVector)
        print("predicted class: ",classNames[predictionList[0]])