from sklearn.feature_extraction.text import CountVectorizer
import numpy as np
import re

def convertDataFrameToListOfList(rawDataFrame):
    #it returns like [['catagoryX','sentence1'],['catagoryY', 'sentence2']]
    columnNames = list(rawDataFrame.columns)
    listOfListFromDF = []
    for index, row in rawDataFrame.iterrows():
        tempList = []
        tempList.append(row[columnNames[0]])
        tempList.append(row[columnNames[1]])
        listOfListFromDF.append(tempList)
        #print(row[columnNames[0]], row[columnNames[1]])
    return listOfListFromDF

'''
def removeStopWords(listOfListFromDF, stopWordList):
    removedStopWordLL =[]
    for twoList in listOfListFromDF:
        sentence = str(twoList[1])
        listOfWords = sentence.split()
        finalSentence = ''
        for word in listOfWords:
            if word not in stopWordList:
                finalSentence+= word+' '
        tempList = []
        tempList.append(twoList[0])
        tempList.append(finalSentence)
        removedStopWordLL.append(tempList)
    return removedStopWordLL
'''

def removePunctuation(sentence):
    sentence = str(sentence)
    sentence = re.sub('[:।;,@#$><A-Za-z0+-9=./''""_০-৯]', '', sentence)
    sentence = re.sub('[?]', ' ? ', sentence)
    sentence = re.sub('[!]', ' ! ', sentence)
    sentence=re.sub(r'(\W)(?=\1)', '', sentence)#Remove Repeated punctuation.........
    return sentence.strip()


def removeStopWords(sentence, stopWordList):
    sentence = str(sentence)
    listOfWords = sentence.split()
    finalSentence = ''
    for word in listOfWords:
        if word not in stopWordList:
            finalSentence += word + ' '
    return finalSentence


def printSentencesOrgAndPrep(originalSentenceList, preprocessedSentence, limit):
    for index in range(limit):
        print("orginal sentence:\n", originalSentenceList[index])
        print("Preprocessed sentence:\n", preprocessedSentence[index])
    return

def getPreprocessedData(listOfListFromDF, stopWordList):
    preprocessedDataList =[]
    originalSentenceList=[]
    preprocessedSentenceList=[]
    for twoList in listOfListFromDF:
        sentence = str(twoList[1])
        originalSentenceList.append(sentence)
        #print(sentence)
        sentence = removePunctuation(sentence)
        finalSentence = removeStopWords(sentence, stopWordList)
        preprocessedSentenceList.append(finalSentence)
        #print(finalSentence)
        tempList = []
        tempList.append(twoList[0])
        tempList.append(finalSentence)
        preprocessedDataList.append(tempList)
    printSentencesOrgAndPrep(originalSentenceList, preprocessedSentenceList, 10)
    return preprocessedDataList


def getDataAndClassFromLL(classAndDtaLL):
    classLabelDict = {'আনন্দ': 0, 'দুঃখ': 1, 'বিরক্ত': 2, 'অবাক': 3, 'ভয়': 4, 'রাগ':5, 'আবেগহীন':6}
    classLabelList=[]
    dataList = []
    for dandcList in classAndDtaLL:
        classLabel = str(dandcList[0])
        classLabel = classLabel.strip()
        classLabel = classLabelDict[classLabel]
        data = str(dandcList[1]).strip()
        classLabelList.append(classLabel)
        dataList.append(data)
    return classLabelList, dataList

def featureExtraction(dataList):
    vectorizer = CountVectorizer(analyzer='word', ngram_range=(1,3), min_df=1, lowercase=False, token_pattern=u'[\S]+',
                          tokenizer=None)
    featuredData = vectorizer.fit_transform(dataList)
    print('number of gram =',len(vectorizer.get_feature_names()))
    mainVocabulary = vectorizer.get_feature_names()
    #print(featuredDataArray.toarray())
    featuredDataArray = featuredData.toarray()
    featuredDataList = featuredDataArray.tolist()
    #print(len(featuredDataList))
    return featuredDataList, mainVocabulary